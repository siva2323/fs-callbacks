const path = require("path")
const fs = require("fs")


function problem1(numberOfFilesToCreate) 
{
    const folderPath = path.join(__dirname, './dummy')

    fs.mkdir(folderPath,  (err) => 
    {
        if (err) 
        {
            throw (err)
        } else 
        {
            for (let index = 0; index < numberOfFilesToCreate; index++) 
            {

                let currentFileName = "random"+index+1;   
                fs.writeFile(path.join(__dirname, "./dummy/"+currentFileName),"message inside the file","utf8", (err) => 
                    {
                        if (err) 
                        {
                           throw (err)
                        } else 
                        {
                            fs.unlink(path.join(folderPath, currentFileName), (err) => 
                            {
                                if (err) 
                                {
                                    throw (err)
                                } 
                            })
                        }
                    })
            }
        }
    })

}

module.exports = problem1